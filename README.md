Task:

1. Browse products in a list. 
2. Add and remove a product to their shopping cart, as long as it is available. 3. Go back to products list view and keep browsing. 
4. Add another product to their cart. 
5. Go to checkout and view their cart. 
6. Modify quantity and remove the product on the checkout page. 

**Testing is required

**Example for provided data in data.json file

self notes:


install server:
nmp init -t
npm i express

run server:
node server.js

install sass:
npm install -g sass

watch sass:
sass --watch ./frontend/static/sass/input.scss ./frontend/static/css/style.css

install jest:
npm install --save-dev jest

run jest:
CD to js folder
node test

// to compile js
install browserify:
npm install -g browserify

compile:
CD to js folder
browserify index.js -o app.js

// install puppeteer:
npm i puppeteer
