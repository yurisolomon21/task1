//get all tests in one file in order to use the same vars

const puppeteer = require('puppeteer');

const addToCart = require('./addToCart.js');
const {
    checkAvailability,
    updateAmountInCart} = require('./cart.js');
const {
    checkExtraOption} = require('./PDP.js');

// PDP
test('expect to get true', () => {
    const checkExtraOption1 = checkExtraOption('{"option": []}');
    expect(checkExtraOption1).toBeTruthy();
});

test('expect to get flase', () => {
    const checkExtraOption2 = checkExtraOption(undefined);
    expect(checkExtraOption2).toBeFalsy();
});

// addToCart
test('add item to cart', () => {
    // add an already in cart item
    const addToCart1 = addToCart(exampleDataItem1, formQuery1, [cartItem1]);
    expect(addToCart1).toStrictEqual([{
        "id": 1,
        "name": "",
        "brand": "",
        "color": "white",
        "optionValue": "5",
        "optionName": "power",
        "amount": 3,
        "quantity": 3,
        "weight": 0,
        "price": 50,
        "totalPrice": 150
    }]);

    // add empty form item to cart
    const addToCart2 = addToCart(exampleDataItem1, {}, [cartItem1]);
    expect(addToCart2).toStrictEqual([cartItem1]);

    // add new item to empty cart
    const addToCart3 = addToCart(exampleDataItem1, formQuery1, []);
    expect(addToCart3).toStrictEqual([{
        "id": 1,
        "name": "",
        "brand": "",
        "color": "white",
        "optionValue": "5",
        "optionName": "power",
        "amount": 1,
        "quantity": 3,
        "weight": 0,
        "price": 50,
        "totalPrice": 50
    }]);

    // add highter amount to cart than quantity
    const addToCart4 = addToCart(exampleDataItem1, formQuery2, []);
    expect(addToCart4).toStrictEqual([]);

});

// cart
test('change quantity in cart' ,() => {
    // reduce quantity
    const updateAmountInCart1 = updateAmountInCart(cartItem1, 1, [cartItem1]);
    expect(updateAmountInCart1).toStrictEqual([{
        "id": 1,
        "name": "",
        "brand": "",
        "color": "white",
        "optionValue": "5",
        "optionName": "power",
        "amount": 1,
        "quantity": 3,
        "weight": 0,
        "price": 50,
        "totalPrice": 50
    }]);

    // increase quantity
    const updateAmountInCart2 = updateAmountInCart(cartItem1, 3, [cartItem1]);
    expect(updateAmountInCart2).toStrictEqual([{
        "id": 1,
        "name": "",
        "brand": "",
        "color": "white",
        "optionValue": "5",
        "optionName": "power",
        "amount": 3,
        "quantity": 3,
        "weight": 0,
        "price": 50,
        "totalPrice": 150
    }]);

    // add quantity more than stock
    const updateAmountInCart3 = updateAmountInCart(cartItem1, 4, [cartItem1]);
    expect(updateAmountInCart3).toStrictEqual([cartItem1]);

    // set amount back to 2, not sure why the vaiable change when useing tests
    const updateAmountInCart4 = updateAmountInCart(cartItem1, 2, [cartItem1]);
    expect(updateAmountInCart4).toStrictEqual([cartItem1]);
});

test('check item availability' ,() => {
    // check for different options for same color
    const checkAvailability1 = checkAvailability(exampleDataItem1, "white", [cartItem1]);
    expect(checkAvailability1).toBe(1);

    // check for same variation
    const checkAvailability2 = checkAvailability(exampleDataItem1, "white", [cartItem2]);
    expect(checkAvailability2).toBe(2);

    const checkAvailability3 = checkAvailability(exampleDataItem1, "white", [cartItem3]);
    expect(checkAvailability3).toBe('AIC');

    const checkAvailability4 = checkAvailability(exampleDataItem2, "white", []);
    expect(checkAvailability4).toBe('OOS');
});

var exampleDataItem1 = {
    "id": 1,
    "name": "",
    "brand": "",
    "price": 50,
    "weight": 0,
    "options": [
        {
            "color": "white",
            "option1": [
                5,
                10
            ],
            "quantity": 3
        },
        {
            "color": "red",
            "option1": [
                5,
                10
            ],
            "quantity": 7
        }
    ],
    "available": true
};

var exampleDataItem2 = {
    "id": 1,
    "name": "",
    "brand": "",
    "price": 50,
    "weight": 0,
    "options": [
        {
            "color": "white",
            "option1": [
                5,
                10
            ],
            "quantity": 0
        },
        {
            "color": "red",
            "option1": [
                5,
                10
            ],
            "quantity": 7
        }
    ],
    "available": true
};

var formQuery1 = {
    "color": "white",
    "power": "5",
    "quantity": "1"
};


var formQuery2 = {
    "color": "white",
    "quantity": "4"
};

var cartItem1 = {
    "id": 1,
    "name": "",
    "brand": "",
    "color": "white",
    "optionValue": "5",
    "optionName": "power",
    "amount": 2,
    "quantity": 3,
    "weight": 0,
    "price": 50,
    "totalPrice": 100
};

var cartItem2 = {
    "id": 1,
    "name": "",
    "brand": "",
    "color": "white",
    "optionValue": "10",
    "optionName": "power",
    "amount": 1,
    "quantity": 3,
    "weight": 0,
    "price": 50,
    "totalPrice": 50
};


var cartItem3 = {
    "id": 1,
    "name": "",
    "brand": "",
    "color": "white",
    "optionValue": "10",
    "optionName": "power",
    "amount": 3,
    "quantity": 3,
    "weight": 0,
    "price": 50,
    "totalPrice": 50
};

// end to end

test('ETE - add item to cart, change amount in cart, remove item from cart', async () => {
    // add to cart
    const browser = await puppeteer.launch({
        headless: false,
        // slowMo: 300,
        args: ['--window-size=1920,1080']
        });
    const page = await browser.newPage()
    await page.goto('http://localhost:4040/');
    await page.waitForSelector('#product-1');
    await page.click('#product-1');
    await page.select('#color', 'white')
    await page.select('#power', '9.5')
    await page.select('#quantity', '3')
    await page.click('.add-to-cart');
    const finalText1 = await page.$eval('#message', el => el.textContent);
    expect(finalText1).toBe('Item was added to cart');

    // change amount in cart
    await page.click('.popup-close');
    await page.click('#cart');
    await page.waitForSelector('.amount');
    await page.select('.amount', '1');
    await page.waitForSelector('.amount');
    const finalAmount = await page.$eval('.amount', el => el.value);
    expect(finalAmount).toBe('1');

    //remove item from cart
    await page.waitForSelector('.remove-from-cart');
    await page.click('.remove-from-cart');
    const finalCart = await page.$eval('.cart-list h3', el => el.textContent);
    expect(finalCart).toBe('Cart is empty');

    await browser.close();
},7000)

test('ETE - add sevral items to cart', async () => {
    const browser = await puppeteer.launch({
        headless: false,
        // slowMo: 300,
        args: ['--window-size=1920,1080']
    });
    const page = await browser.newPage()
    await page.goto('http://localhost:4040/');
    await page.waitForSelector('#product-1');
    await page.click('#product-2');

    // get please choose a color
    await page.click('.add-to-cart');
    const finalText2 = await page.$eval('#message', el => el.textContent);
    expect(finalText2).toBe('Please choose a color');
    
    // add item to cart
    await page.select('#color', 'white');
    await page.select('#power', '9.5');
    await page.select('#quantity', '2');
    await page.click('.add-to-cart');
    const finalText3 = await page.$eval('#message', el => el.textContent);
    expect(finalText3).toBe('Item was added to cart');

    // add second item
    await page.select('#power', '6.5');
    await page.select('#quantity', '1');
    await page.click('.add-to-cart');
    const finalText4 = await page.$eval('#message', el => el.textContent);
    expect(finalText4).toBe('Item was added to cart');

    // get AIC
    await page.click('.add-to-cart');
    const finalText5 = await page.$eval('#message', el => el.textContent);
    expect(finalText5).toBe('All available items of this color are in cart');

    // get OOS
    await page.click('.popup-close');
    await page.click('#product-4');
    await page.select('#color', 'black');
    await page.click('.add-to-cart');
    const finalText6 = await page.$eval('#message', el => el.textContent);
    expect(finalText6).toBe('Item is out of stock');

    // add final item
    await page.select('#color', 'white');
    await page.select('#quantity', '2');
    await page.click('.add-to-cart');
    const finalText7 = await page.$eval('#message', el => el.textContent);
    expect(finalText7).toBe('Item was added to cart');

    const finalText8 = await page.$eval('#items-in-cart', el => el.textContent);
    expect(finalText8).toBe('5');

    await browser.close();
},7000)
