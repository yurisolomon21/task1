function openPDP(itemInView) {
    let template = 
    `
    <div class="PDP-wrapper">
        <h1>${itemInView.name}</h1>
        <h3>Brand: ${itemInView.brand}</h3>
        <div class="details">
            <form id="PDP-select">
                <label for="color">Choose a color:</label>
                    <select name="color" id="color">
                    <option></option>
    `
    // create color list
    itemInView.options.forEach(option => {
        if (option.color) {
            template +=
            `
            <option value="${option.color}">${option.color}</option>
            `
        }
    })
    // check if options exist and calculate them in
    let hasPowerOption = false;
    let hasStorageOption = false;
    itemInView.options.forEach(option => {
        let power = option.power;
        let storage = option.storage;

        hasPowerOption = checkExtraOption(power);
        hasStorageOption = checkExtraOption(storage);
    })
    // not handeling with function boolean to avoide editing the template inside a loop
    template += addOption(hasPowerOption, "power");
    template += addOption(hasStorageOption, "storage");

    template +=
    `
                </select> </br>

                <label for="quantity">Quantity:</label>
                <select name="quantity" id="quantity" disabled>
                    <option></option>
                </select> </br>
                <p>Weight: ${itemInView.weight}</p>
                <p id="message"></p>
                <p>Price: <span id="product-price">${itemInView.price} per 1 piece</span></p>
                <p class="add-to-cart button-black" data-addtocart-id="${itemInView.id}">Add To Cart</p>
            </form>
        </div>
    </div>
    `;

    return template;
}

// looking for active color and updating options
function validateVariants(itemInView, color) {
    itemInView.options.forEach(option => {
        if (option.color == color) {
            let power = option.power;
            let storage = option.storage;

            updateForm(power, "power");
            updateForm(storage, "storage");
        } else if (color == "") {
            if (document.getElementById('power') || document.getElementById('storage')) {
                document.getElementById('power').innerHTML = `<option></option>`;
                document.getElementById('power').disabled = true;
            }
        }
    })
};

// update form with new options based on chosen color
function updateForm(option, id) {
    if (option !== undefined) {
        let template;
        option.forEach(elem => {
            template +=
            `
            <option value="${elem}">${elem}</option>
            `
        })
        document.getElementById(id).disabled = false;
        document.getElementById(id).innerHTML = template;                
    }
}

// check if more options exist, return boolean
function checkExtraOption(option) {
    if (option && option.length !== 0) {
        return true;
    } else {
        return false;
    }
}

// add the form options
function addOption(hasOption, optionName) {
    let template;
    if (hasOption) {
        template =
        `
        </select> </br>
        <label for="${optionName}">Choose ${optionName}:</label>
        <select name="${optionName}" id="${optionName}" disabled>
            <option></option>
        `;
        return template;
    }
}

// update available quantity into select form
function updateAvailability(stock) {
    document.getElementById('quantity').innerHTML='';
    let template;
    if (stock > 0) {
        for (let i = 0; i < stock; i++) {
            template +=
            `
            <option value="${i+1}">${i+1}</option>
            `;
        }
        document.getElementById('quantity').disabled = false;
    } else {
        template +=
        `
        <option></option>
        `;
        document.getElementById('quantity').disabled = true;
    }
    document.getElementById('quantity').innerHTML=template;                
}

module.exports = {
    openPDP,
    updateAvailability,
    validateVariants,
    checkExtraOption
}
