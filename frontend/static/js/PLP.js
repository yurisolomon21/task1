function printList(data, template) {
    template +=
    `
    <div class="PLP">
    `;
    data.items.forEach(item => {
        template +=
            `
            <div class="product-tile popup-trigger" data-product-id="${item.id}" id="product-${item.id}">
            `;

        template +=
            `
            <h3 class="product-title">
                ${item.name}
            </h3>
            <div class="product-details">
            `;

        let colors = [];
        item.options.forEach(option => {
            if (option.color) {
                colors.push(option.color)
            }
        });

        if (colors.length !== 0) {
            template +=
                `
                <p class="color-list">
                    <b>Available in:</b> ${colors}
                </p>
                `;
        }

        template +=
            `
                <p class="product-price"><b>Price:</b> ${item.price}</p>
                </div>
            </div>
            `;
    });
    template +=
    `
    </div>
    `;

    return template;
};

module.exports = printList;
