function addToCart(itemToAdd, formQuery, cart) {
    let itemJson;
    let optionKey;
    let optionValue;
    let existInCart = false;
    
    if (formQuery.power) {
        optionKey = "power";
        optionValue =  formQuery.power;
    }
    if (formQuery.storage) {
        optionKey = "storage";
        optionValue =  formQuery.storage;
    }

    if (cart.length !== 0) {
        cart.forEach(item => {
            if (item.id == itemToAdd.id && item.color == formQuery.color) {
                if (optionValue == item.optionValue) {
                    existInCart = true;
                    if (item.amount + parseInt(formQuery.quantity) <= item.quantity) {
                        let newAmount = item.amount + parseInt(formQuery.quantity);
                        item.amount=newAmount;
                        // item.totalWeight = newAmount * item.weight;
                        item.totalPrice = newAmount * item.price;
                    }
                }
            }
        })
    }
    
    if (!existInCart) {
        itemToAdd.options.forEach(option => {
            if (option.color == formQuery.color) {
                if (isNaN(Number(option.quantity)) || option.quantity == 0 || formQuery.quantity > option.quantity) {
                    return;
                }
                // let totalWeight = parseInt(formQuery.quantity) * Number(itemToAdd.weight);
                let totalPrice = parseInt(formQuery.quantity) * parseInt(itemToAdd.price);
                itemJson=
                {   
                    "id": itemToAdd.id,
                    "name": itemToAdd.name,
                    "brand": itemToAdd.brand,
                    "color": formQuery.color,
                    "optionValue": optionValue,
                    "optionName": optionKey,
                    "amount": parseInt(formQuery.quantity),
                    "quantity": parseInt(option.quantity),
                    "weight": Number(itemToAdd.weight),
                    "price": parseInt(itemToAdd.price),
                    "totalPrice": totalPrice,
                }
                // "totalWeight": totalWeight,
            }
        });
        if (itemJson !== undefined) {
            cart.push(itemJson)
            
        }
    }
    return cart;
}

module.exports = addToCart;
