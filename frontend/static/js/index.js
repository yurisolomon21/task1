const addToCart = require('./addToCart.js');
const {
    checkAvailability,
    getTotalItems,
    renderCart,
    updateAmountInCart} = require('./cart.js');
const {
    openPDP,
    updateAvailability,
    validateVariants} = require('./PDP.js');
const printList = require('./PLP.js');


let cartList;
if (localStorage.getItem("cartList") !== null) {
    cartList = JSON.parse(localStorage.getItem("cartList"));
    if (cartList.length > 0) {
        getTotalItems(cartList);
    }
} else {
    cartList = []
}
var cart = [];

(function getData() {
    var data;
    if (localStorage.getItem("data") !== null) {
        data = JSON.parse(localStorage.getItem("data"));
        getItems(data);
        events(data)
    } else {
        // fetch("http://myjson.dit.upm.es/api/bins/byfb", {
        fetch("https://api.npoint.io/0bcb35ed6a20e0c48a31", {
        })
        .then(response => response.json())
        .then(realData => {
            
            var fetchedData = realData;
            localStorage.setItem('data', JSON.stringify(fetchedData));
            data = fetchedData;
            getItems(data);
            events(data)
        }).catch(function(error) {
            document.getElementById('populateBody').innerHTML = `<h3>Couldn't load data, please refresh or try again later`;
            // console.log(error);
        });;
    };
})();

function getItems(data) {
    let template = ``;

    if(window.location.pathname == "/") {
        template = printList(data, template);
    } else if(window.location.pathname == "/cart") {
        template = renderCart(cartList);
    }
    document.getElementById('populateBody').innerHTML = template;
}

function events(data) {
    // if(window.location.pathname == "/") {
        document.querySelectorAll('.popup-trigger').forEach(trigger => {

        trigger.addEventListener('click', function (e) {
            if (e.target.classList.contains('amount') || e.target.classList.contains('remove-from-cart')) {
                return;
            }
            // add class open to open popup
            let target = e.target.closest('.popup-trigger');
            document.getElementById('popup').classList.add('open');
            // get clicked item to send to PDP.js
            let itemInView;
            data.items.forEach(item => {
                if (item.id == target.getAttribute("data-product-id")) {
                    itemInView = item;
                }
            });
            let template = openPDP(itemInView);
            // populate popup content
            document.getElementById('popup').querySelector('.popup-content').innerHTML=template;
        

            // next events are inside this one because they are depandent on the HTML that is being populated in this event


            // handle add to cart
            document.querySelector('.add-to-cart').addEventListener('click', function (e) {
                // get data for active item
                let itemToAdd;
                data.items.forEach(item => {
                    if (item.id == e.target.getAttribute('data-addtocart-id')) {
                        itemToAdd = item;
                    }
                });
        
                // get form data about variants and quantity
                let formQuery = Array.from(document.querySelectorAll('form select')).reduce((acc, input) => ({ ...acc, [input.id]: input.value }), {});

                let stock = checkAvailability(itemToAdd, formQuery.color, cart);
                if (formQuery.color == '') {
                    document.getElementById('message').innerHTML = "Please choose a color";
                    document.getElementById('message').classList.add('error');
                } else if (stock == "OOS") {
                    document.getElementById('message').innerHTML = "Item is out of stock";
                    document.getElementById('message').classList.add('error');
                } else if (stock == "AIC") {
                    document.getElementById('message').innerHTML = "All available items of this color are in cart";
                    document.getElementById('message').classList.add('error');
                } else if (stock > 0) {
                    // update cart
                    cart = addToCart(itemToAdd, formQuery, cartList);
        
                    stock = checkAvailability(itemToAdd, formQuery.color, cart);
                    updateAvailability(stock);
        
                    // save items to list as a string
                    localStorage.setItem('cartList', JSON.stringify(cart));
                    getTotalItems(cart);
                    document.getElementById('message').innerHTML = "Item was added to cart";
                    document.getElementById('message').classList.add('success');

                    if (window.location.pathname == "/cart") {
                        setTimeout(() => {
                            location.reload();
                        }, 3000);
                    };
                    
                } else {
                    document.getElementById('message').innerHTML = "Unexpected error";
                    document.getElementById('message').classList.add('error');
                }
                setTimeout(function(){
                    document.getElementById('message').innerHTML = "",
                    document.getElementById('message').classList = ""
                },3000);
            });

        document.querySelector('.popup-close').addEventListener('click', function () {
            document.getElementById('popup').classList.remove('open');
        });

        document.getElementById('popup').addEventListener('click', function (e) {
            if (e.target.classList.contains('open')) {
                document.getElementById('popup').classList.remove('open');
            }
        });

        //listen to form change and update form
        document.getElementById('color').addEventListener('change', function () {
            var color = document.getElementById('color').value;
            validateVariants(itemInView, color);
            let stock = checkAvailability(itemInView, color, cartList);
            updateAvailability(stock);
        });
    
        });
    });

    // } else 
    if (window.location.pathname == "/cart") {
        if (cartList.length > 0) {
            
            document.querySelectorAll('.remove-from-cart').forEach(cartItem => {
                cartItem.addEventListener('click', function (e) {
                    let itemToRemove = JSON.parse(e.target.closest('.cart-item').getAttribute('data-item'));
                    cartList.forEach(item => {
                        if (item.id == itemToRemove.id && item.color == itemToRemove.color) {
                            if (itemToRemove.optionValue == item.optionValue || itemToRemove.optionValue == "undefined") {
                                cartList.splice(cartList.indexOf(item), 1);
                                // save items to list as a string
                                localStorage.setItem('cartList', JSON.stringify(cartList));
                                location.reload();
                            }
                        }
                    })
                })
            });

            document.querySelector('.remove-all-from-cart').addEventListener('click', function (e) {
                cart = [];
                
                // save items to list as a string
                localStorage.setItem('cartList', JSON.stringify(cart));
                location.reload();
            });
            
            document.querySelectorAll('.amount').forEach(itemInCart => {
                itemInCart.addEventListener('change', function (e) {
                    cart = updateAmountInCart(JSON.parse(e.target.closest('.cart-item').getAttribute('data-item')), e.target.value, cartList);
                    // save items to list as a string
                    localStorage.setItem('cartList', JSON.stringify(cart));
                    location.reload();
                })
            })
        }
    }
}
