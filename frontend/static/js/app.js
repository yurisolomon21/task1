(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
function openPDP(itemInView) {
    let template = 
    `
    <div class="PDP-wrapper">
        <h1>${itemInView.name}</h1>
        <h3>Brand: ${itemInView.brand}</h3>
        <div class="details">
            <form id="PDP-select">
                <label for="color">Choose a color:</label>
                    <select name="color" id="color">
                    <option></option>
    `
    // create color list
    itemInView.options.forEach(option => {
        if (option.color) {
            template +=
            `
            <option value="${option.color}">${option.color}</option>
            `
        }
    })
    // check if options exist and calculate them in
    let hasPowerOption = false;
    let hasStorageOption = false;
    itemInView.options.forEach(option => {
        let power = option.power;
        let storage = option.storage;

        hasPowerOption = checkExtraOption(power);
        hasStorageOption = checkExtraOption(storage);
    })
    // not handeling with function boolean to avoide editing the template inside a loop
    template += addOption(hasPowerOption, "power");
    template += addOption(hasStorageOption, "storage");

    template +=
    `
                </select> </br>

                <label for="quantity">Quantity:</label>
                <select name="quantity" id="quantity" disabled>
                    <option></option>
                </select> </br>
                <p>Weight: ${itemInView.weight}</p>
                <p id="message"></p>
                <p>Price: <span id="product-price">${itemInView.price} per 1 piece</span></p>
                <p class="add-to-cart button-black" data-addtocart-id="${itemInView.id}">Add To Cart</p>
            </form>
        </div>
    </div>
    `;

    return template;
}

// looking for active color and updating options
function validateVariants(itemInView, color) {
    itemInView.options.forEach(option => {
        if (option.color == color) {
            let power = option.power;
            let storage = option.storage;

            updateForm(power, "power");
            updateForm(storage, "storage");
        } else if (color == "") {
            if (document.getElementById('power') || document.getElementById('storage')) {
                document.getElementById('power').innerHTML = `<option></option>`;
                document.getElementById('power').disabled = true;
            }
        }
    })
};

// update form with new options based on chosen color
function updateForm(option, id) {
    if (option !== undefined) {
        let template;
        option.forEach(elem => {
            template +=
            `
            <option value="${elem}">${elem}</option>
            `
        })
        document.getElementById(id).disabled = false;
        document.getElementById(id).innerHTML = template;                
    }
}

// check if more options exist, return boolean
function checkExtraOption(option) {
    if (option && option.length !== 0) {
        return true;
    } else {
        return false;
    }
}

// add the form options
function addOption(hasOption, optionName) {
    let template;
    if (hasOption) {
        template =
        `
        </select> </br>
        <label for="${optionName}">Choose ${optionName}:</label>
        <select name="${optionName}" id="${optionName}" disabled>
            <option></option>
        `;
        return template;
    }
}

// update available quantity into select form
function updateAvailability(stock) {
    document.getElementById('quantity').innerHTML='';
    let template;
    if (stock > 0) {
        for (let i = 0; i < stock; i++) {
            template +=
            `
            <option value="${i+1}">${i+1}</option>
            `;
        }
        document.getElementById('quantity').disabled = false;
    } else {
        template +=
        `
        <option></option>
        `;
        document.getElementById('quantity').disabled = true;
    }
    document.getElementById('quantity').innerHTML=template;                
}

module.exports = {
    openPDP,
    updateAvailability,
    validateVariants,
    checkExtraOption
}

},{}],2:[function(require,module,exports){
function printList(data, template) {
    template +=
    `
    <div class="PLP">
    `;
    data.items.forEach(item => {
        template +=
            `
            <div class="product-tile popup-trigger" data-product-id="${item.id}" id="product-${item.id}">
            `;

        template +=
            `
            <h3 class="product-title">
                ${item.name}
            </h3>
            <div class="product-details">
            `;

        let colors = [];
        item.options.forEach(option => {
            if (option.color) {
                colors.push(option.color)
            }
        });

        if (colors.length !== 0) {
            template +=
                `
                <p class="color-list">
                    <b>Available in:</b> ${colors}
                </p>
                `;
        }

        template +=
            `
                <p class="product-price"><b>Price:</b> ${item.price}</p>
                </div>
            </div>
            `;
    });
    template +=
    `
    </div>
    `;

    return template;
};

module.exports = printList;

},{}],3:[function(require,module,exports){
function addToCart(itemToAdd, formQuery, cart) {
    let itemJson;
    let optionKey;
    let optionValue;
    let existInCart = false;
    
    if (formQuery.power) {
        optionKey = "power";
        optionValue =  formQuery.power;
    }
    if (formQuery.storage) {
        optionKey = "storage";
        optionValue =  formQuery.storage;
    }

    if (cart.length !== 0) {
        cart.forEach(item => {
            if (item.id == itemToAdd.id && item.color == formQuery.color) {
                if (optionValue == item.optionValue) {
                    existInCart = true;
                    if (item.amount + parseInt(formQuery.quantity) <= item.quantity) {
                        let newAmount = item.amount + parseInt(formQuery.quantity);
                        item.amount=newAmount;
                        // item.totalWeight = newAmount * item.weight;
                        item.totalPrice = newAmount * item.price;
                    }
                }
            }
        })
    }
    
    if (!existInCart) {
        itemToAdd.options.forEach(option => {
            if (option.color == formQuery.color) {
                if (isNaN(Number(option.quantity)) || option.quantity == 0 || formQuery.quantity > option.quantity) {
                    return;
                }
                // let totalWeight = parseInt(formQuery.quantity) * Number(itemToAdd.weight);
                let totalPrice = parseInt(formQuery.quantity) * parseInt(itemToAdd.price);
                itemJson=
                {   
                    "id": itemToAdd.id,
                    "name": itemToAdd.name,
                    "brand": itemToAdd.brand,
                    "color": formQuery.color,
                    "optionValue": optionValue,
                    "optionName": optionKey,
                    "amount": parseInt(formQuery.quantity),
                    "quantity": parseInt(option.quantity),
                    "weight": Number(itemToAdd.weight),
                    "price": parseInt(itemToAdd.price),
                    "totalPrice": totalPrice,
                }
                // "totalWeight": totalWeight,
            }
        });
        if (itemJson !== undefined) {
            cart.push(itemJson)
            
        }
    }
    return cart;
}

module.exports = addToCart;

},{}],4:[function(require,module,exports){
function renderCart(cart) {
    let template = 
    `
    <div class="cart">
        <div class="cart-list">
    `;
    if (cart.length == 0) {
        template += 
        `
        <h3>Cart is empty</h3>
        `;
    } else {
        // let totalWeight = 0;
        let totalPrice = 0;
        cart.forEach(item => {
            let dataToPass = {"id": `${item.id}`, "color": `${item.color}`, "optionName": `${item.optionName}`, "optionValue": `${item.optionValue}`};
            template += 
            `
            <div class="cart-item popup-trigger" data-item='${JSON.stringify(dataToPass)}' data-product-id="${item.id}">
                <h2>${item.name}</h2>
                <h3>Features:</h3>
                <ul>
                    <li><b>Color:</b> ${item.color}</li>
            `;
            if (item.optionName !== undefined) {
                template += 
                `
                <li><b>${item.optionName}:</b> ${item.optionValue}</li>
                `;
            };
            template += 
                `
                    <li><b>Brand:</b> ${item.brand}</li>
                    <li><b>Weight:</b> ${item.weight}</li>
                </ul>
                <h3>Quantity: </h3>
                <select name="quantity" class="amount">

            `;

            template += updateAmountSelectForm(cart, item);

            template += 
            `
            </select>   
            <h4>Price: ${item.totalPrice}</h4>
            <p class="remove-from-cart button-black">remove from cart</p>
            </div>
            `;
            // totalWeight += Number(item.totalWeight);
            totalPrice += parseInt(item.totalPrice);
        });
        // total weight: ${totalWeight}
        template += 
        `
        <div class="cart-summery">
            <p class="cart-total-price"><b>Total price:</b> ${totalPrice}</p>
            <p class="check-out button-white">Check out</p>
            <p class="remove-all-from-cart button-black">Remove all from cart</p>
        </div>
        `;
    }
        
    template += 
    `
        </div>
    </div>
    `;
    return template
}

//  change chosen amount in cart
function updateAmountInCart(itemData, newAmount, cart) {
    cart.forEach(item => {
        if (item.id == parseInt(itemData.id) && item.color == itemData.color && item.optionValue == itemData.optionValue) {
            if (newAmount <= item.quantity) {
                item.amount=newAmount;
                // item.totalWeight = newAmount * item.weight;
                item.totalPrice = newAmount * item.price;
            }
        }
    })
    return cart;
}

// checking avialability in stock
function checkAvailability(itemInView, color, cart) {
    let availability;
    let inCart = 0;
    let inStock = 0;
    itemInView.options.forEach(option => {
        if (option.color == color) {
            inStock += parseInt(option.quantity);
        }
    });
    if (cart.length !== 0) {
        cart.forEach(item => {
            if (item.id == itemInView.id && item.color == color) {
                inCart += item.amount;
            }
        });
    }
    if (inStock > inCart) {
        availability = inStock - inCart;
    } else if (inStock == 0) {
        availability = "OOS";
    } else if (inStock == inCart) {
        availability = "AIC";
    }

    return availability;
}

// amount of items in cart to be displayes over cart button in nav
function getTotalItems(cart) {
    if (cart.length > 0) {
        let amountInCart = 0;
        cart.forEach(item => {
            amountInCart += parseInt(item.amount);
        })
        if (amountInCart > 0) {
            document.getElementById('items-in-cart').innerHTML = amountInCart;
            document.getElementById('items-in-cart').classList.remove('empty');
        } else {
            document.getElementById('items-in-cart').innerHTML = '';
            document.getElementById('items-in-cart').classList.add('empty');
        }
    }
}

// update available stock to choose in select form
function updateAmountSelectForm(cart, item) {
    let availableStock = item.quantity;
    let template;
    cart.forEach(cartItem => {
        if (item.id == cartItem.id && item.color == cartItem.color && item.optionValue !== cartItem.optionValue) {
            availableStock -= cartItem.amount;
        }
    })
    // quantity check
    for (let i = 0; i < availableStock; i++) {
        if (item.amount == i + 1) {
            template += 
            `
            <option selected value="${i+1}">${i+1}</option>
            `;
        } else {
            template += 
            `
            <option value="${i+1}">${i+1}</option>
            `;
        }
    }
    return template;
}

module.exports = {
    updateAmountSelectForm,
    checkAvailability,
    getTotalItems,
    renderCart,
    updateAmountInCart
}

},{}],5:[function(require,module,exports){
const addToCart = require('./addToCart.js');
const {
    checkAvailability,
    getTotalItems,
    renderCart,
    updateAmountInCart} = require('./cart.js');
const {
    openPDP,
    updateAvailability,
    validateVariants} = require('./PDP.js');
const printList = require('./PLP.js');


let cartList;
if (localStorage.getItem("cartList") !== null) {
    cartList = JSON.parse(localStorage.getItem("cartList"));
    if (cartList.length > 0) {
        getTotalItems(cartList);
    }
} else {
    cartList = []
}
var cart = [];

(function getData() {
    var data;
    if (localStorage.getItem("data") !== null) {
        data = JSON.parse(localStorage.getItem("data"));
        getItems(data);
        events(data)
    } else {
        // fetch("http://myjson.dit.upm.es/api/bins/byfb", {
        fetch("https://api.npoint.io/0bcb35ed6a20e0c48a31", {
        })
        .then(response => response.json())
        .then(realData => {
            
            var fetchedData = realData;
            localStorage.setItem('data', JSON.stringify(fetchedData));
            data = fetchedData;
            getItems(data);
            events(data)
        }).catch(function(error) {
            document.getElementById('populateBody').innerHTML = `<h3>Couldn't load data, please refresh or try again later`;
            // console.log(error);
        });;
    };
})();

function getItems(data) {
    let template = ``;

    if(window.location.pathname == "/") {
        template = printList(data, template);
    } else if(window.location.pathname == "/cart") {
        template = renderCart(cartList);
    }
    document.getElementById('populateBody').innerHTML = template;
}

function events(data) {
    // if(window.location.pathname == "/") {
        document.querySelectorAll('.popup-trigger').forEach(trigger => {

        trigger.addEventListener('click', function (e) {
            if (e.target.classList.contains('amount') || e.target.classList.contains('remove-from-cart')) {
                return;
            }
            // add class open to open popup
            let target = e.target.closest('.popup-trigger');
            document.getElementById('popup').classList.add('open');
            // get clicked item to send to PDP.js
            let itemInView;
            data.items.forEach(item => {
                if (item.id == target.getAttribute("data-product-id")) {
                    itemInView = item;
                }
            });
            let template = openPDP(itemInView);
            // populate popup content
            document.getElementById('popup').querySelector('.popup-content').innerHTML=template;
        

            // next events are inside this one because they are depandent on the HTML that is being populated in this event


            // handle add to cart
            document.querySelector('.add-to-cart').addEventListener('click', function (e) {
                // get data for active item
                let itemToAdd;
                data.items.forEach(item => {
                    if (item.id == e.target.getAttribute('data-addtocart-id')) {
                        itemToAdd = item;
                    }
                });
        
                // get form data about variants and quantity
                let formQuery = Array.from(document.querySelectorAll('form select')).reduce((acc, input) => ({ ...acc, [input.id]: input.value }), {});

                let stock = checkAvailability(itemToAdd, formQuery.color, cart);
                if (formQuery.color == '') {
                    document.getElementById('message').innerHTML = "Please choose a color";
                    document.getElementById('message').classList.add('error');
                } else if (stock == "OOS") {
                    document.getElementById('message').innerHTML = "Item is out of stock";
                    document.getElementById('message').classList.add('error');
                } else if (stock == "AIC") {
                    document.getElementById('message').innerHTML = "All available items of this color are in cart";
                    document.getElementById('message').classList.add('error');
                } else if (stock > 0) {
                    // update cart
                    cart = addToCart(itemToAdd, formQuery, cartList);
        
                    stock = checkAvailability(itemToAdd, formQuery.color, cart);
                    updateAvailability(stock);
        
                    // save items to list as a string
                    localStorage.setItem('cartList', JSON.stringify(cart));
                    getTotalItems(cart);
                    document.getElementById('message').innerHTML = "Item was added to cart";
                    document.getElementById('message').classList.add('success');

                    if (window.location.pathname == "/cart") {
                        setTimeout(() => {
                            location.reload();
                        }, 3000);
                    };
                    
                } else {
                    document.getElementById('message').innerHTML = "Unexpected error";
                    document.getElementById('message').classList.add('error');
                }
                setTimeout(function(){
                    document.getElementById('message').innerHTML = "",
                    document.getElementById('message').classList = ""
                },3000);
            });

        document.querySelector('.popup-close').addEventListener('click', function () {
            document.getElementById('popup').classList.remove('open');
        });

        document.getElementById('popup').addEventListener('click', function (e) {
            if (e.target.classList.contains('open')) {
                document.getElementById('popup').classList.remove('open');
            }
        });

        //listen to form change and update form
        document.getElementById('color').addEventListener('change', function () {
            var color = document.getElementById('color').value;
            validateVariants(itemInView, color);
            let stock = checkAvailability(itemInView, color, cartList);
            updateAvailability(stock);
        });
    
        });
    });

    // } else 
    if (window.location.pathname == "/cart") {
        if (cartList.length > 0) {
            
            document.querySelectorAll('.remove-from-cart').forEach(cartItem => {
                cartItem.addEventListener('click', function (e) {
                    let itemToRemove = JSON.parse(e.target.closest('.cart-item').getAttribute('data-item'));
                    cartList.forEach(item => {
                        if (item.id == itemToRemove.id && item.color == itemToRemove.color) {
                            if (itemToRemove.optionValue == item.optionValue || itemToRemove.optionValue == "undefined") {
                                cartList.splice(cartList.indexOf(item), 1);
                                // save items to list as a string
                                localStorage.setItem('cartList', JSON.stringify(cartList));
                                location.reload();
                            }
                        }
                    })
                })
            });

            document.querySelector('.remove-all-from-cart').addEventListener('click', function (e) {
                cart = [];
                
                // save items to list as a string
                localStorage.setItem('cartList', JSON.stringify(cart));
                location.reload();
            });
            
            document.querySelectorAll('.amount').forEach(itemInCart => {
                itemInCart.addEventListener('change', function (e) {
                    cart = updateAmountInCart(JSON.parse(e.target.closest('.cart-item').getAttribute('data-item')), e.target.value, cartList);
                    // save items to list as a string
                    localStorage.setItem('cartList', JSON.stringify(cart));
                    location.reload();
                })
            })
        }
    }
}

},{"./PDP.js":1,"./PLP.js":2,"./addToCart.js":3,"./cart.js":4}]},{},[5]);
