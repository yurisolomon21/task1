function renderCart(cart) {
    let template = 
    `
    <div class="cart">
        <div class="cart-list">
    `;
    if (cart.length == 0) {
        template += 
        `
        <h3>Cart is empty</h3>
        `;
    } else {
        // let totalWeight = 0;
        let totalPrice = 0;
        cart.forEach(item => {
            let dataToPass = {"id": `${item.id}`, "color": `${item.color}`, "optionName": `${item.optionName}`, "optionValue": `${item.optionValue}`};
            template += 
            `
            <div class="cart-item popup-trigger" data-item='${JSON.stringify(dataToPass)}' data-product-id="${item.id}">
                <h2>${item.name}</h2>
                <h3>Features:</h3>
                <ul>
                    <li><b>Color:</b> ${item.color}</li>
            `;
            if (item.optionName !== undefined) {
                template += 
                `
                <li><b>${item.optionName}:</b> ${item.optionValue}</li>
                `;
            };
            template += 
                `
                    <li><b>Brand:</b> ${item.brand}</li>
                    <li><b>Weight:</b> ${item.weight}</li>
                </ul>
                <h3>Quantity: </h3>
                <select name="quantity" class="amount">

            `;

            template += updateAmountSelectForm(cart, item);

            template += 
            `
            </select>   
            <h4>Price: ${item.totalPrice}</h4>
            <p class="remove-from-cart button-black">remove from cart</p>
            </div>
            `;
            // totalWeight += Number(item.totalWeight);
            totalPrice += parseInt(item.totalPrice);
        });
        // total weight: ${totalWeight}
        template += 
        `
        <div class="cart-summery">
            <p class="cart-total-price"><b>Total price:</b> ${totalPrice}</p>
            <p class="check-out button-white">Check out</p>
            <p class="remove-all-from-cart button-black">Remove all from cart</p>
        </div>
        `;
    }
        
    template += 
    `
        </div>
    </div>
    `;
    return template
}

//  change chosen amount in cart
function updateAmountInCart(itemData, newAmount, cart) {
    cart.forEach(item => {
        if (item.id == parseInt(itemData.id) && item.color == itemData.color && item.optionValue == itemData.optionValue) {
            if (newAmount <= item.quantity) {
                item.amount=newAmount;
                // item.totalWeight = newAmount * item.weight;
                item.totalPrice = newAmount * item.price;
            }
        }
    })
    return cart;
}

// checking avialability in stock
function checkAvailability(itemInView, color, cart) {
    let availability;
    let inCart = 0;
    let inStock = 0;
    itemInView.options.forEach(option => {
        if (option.color == color) {
            inStock += parseInt(option.quantity);
        }
    });
    if (cart.length !== 0) {
        cart.forEach(item => {
            if (item.id == itemInView.id && item.color == color) {
                inCart += item.amount;
            }
        });
    }
    if (inStock > inCart) {
        availability = inStock - inCart;
    } else if (inStock == 0) {
        availability = "OOS";
    } else if (inStock == inCart) {
        availability = "AIC";
    }

    return availability;
}

// amount of items in cart to be displayes over cart button in nav
function getTotalItems(cart) {
    if (cart.length > 0) {
        let amountInCart = 0;
        cart.forEach(item => {
            amountInCart += parseInt(item.amount);
        })
        if (amountInCart > 0) {
            document.getElementById('items-in-cart').innerHTML = amountInCart;
            document.getElementById('items-in-cart').classList.remove('empty');
        } else {
            document.getElementById('items-in-cart').innerHTML = '';
            document.getElementById('items-in-cart').classList.add('empty');
        }
    }
}

// update available stock to choose in select form
function updateAmountSelectForm(cart, item) {
    let availableStock = item.quantity;
    let template;
    cart.forEach(cartItem => {
        if (item.id == cartItem.id && item.color == cartItem.color && item.optionValue !== cartItem.optionValue) {
            availableStock -= cartItem.amount;
        }
    })
    // quantity check
    for (let i = 0; i < availableStock; i++) {
        if (item.amount == i + 1) {
            template += 
            `
            <option selected value="${i+1}">${i+1}</option>
            `;
        } else {
            template += 
            `
            <option value="${i+1}">${i+1}</option>
            `;
        }
    }
    return template;
}

module.exports = {
    updateAmountSelectForm,
    checkAvailability,
    getTotalItems,
    renderCart,
    updateAmountInCart
}
